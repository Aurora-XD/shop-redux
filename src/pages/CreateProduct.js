import React from "react";
import {bindActionCreators} from "redux";
import {
  createProductImageUrl,
  createProductName,
  createProductPrice,
  createProductUnit,
  postProduct
} from "../actions/postProduct";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";


class CreateProduct extends React.Component{

  constructor(props){
    super(props)
    this.handleCreateName = this.handleCreateName.bind(this)
    this.handleCreatePrice = this.handleCreatePrice.bind(this)
    this.handleCreateUnit = this.handleCreateUnit.bind(this)
    this.handleCreateImage = this.handleCreateImage.bind(this)
    this.handleCreateProduct = this.handleCreateProduct.bind(this)
  }

  handleCreateName(event){
    this.props.createProductName(event.target.value)
  }

  handleCreatePrice(event){
    this.props.createProductPrice(event.target.value)
  }

  handleCreateUnit(event){
    this.props.createProductUnit(event.target.value)
  }

  handleCreateImage(event){
    this.props.createProductImageUrl(event.target.value)
  }

  handleCreateProduct(name,price,unit,image){
    this.props.postProduct(name,price,unit,image)
  }

  render() {
    return (
      <div>
        <section className={'createProduct'}>
          <h1>添加商品</h1>

          <div>
            <label className={'title'}>名称</label>
            <input className={'content'} onChange={this.handleCreateName} type="text"/>
          </div>
          <div>
            <label className={'title'}>价格</label>
            <input className={'content'} onChange={this.handleCreatePrice} type="text"/>
          </div>
          <div>
            <label className={'title'}>单位</label>
            <input className={'content'} onChange={this.handleCreateUnit} type="text"/>
          </div>
          <div>
            <label className={'title'}>图片</label>
            <input className={'content'} onChange={this.handleCreateImage} type="text"/>
          </div>

          <div>
            <NavLink to='/'>
              <button className={'post'} onClick={()=>{
                this.handleCreateProduct(this.props,this.props.name,this.props.price,this.props.unit,this.props.image)
              }}>提交</button>
            </NavLink>
          </div>
        </section>
      </div>
    );
  }
}

const mapStatesToProps = state => ({
  name: state.createProductReducer.name,
  price: state.createProductReducer.price,
  unit: state.createProductReducer.unit,
  image: state.createProductReducer.image
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createProductName,createProductPrice,createProductUnit,createProductImageUrl,postProduct
},dispatch);

export default connect(mapStatesToProps,mapDispatchToProps)(CreateProduct);