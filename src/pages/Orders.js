import React from "react";
import {bindActionCreators} from "redux";
import {getOrders} from "../actions/getOrders";
import {connect} from "react-redux";
import OrderItem from "../component/OrderItem";
import {getProducts} from "../actions/getProducts";
import {deleteOrder} from "../actions/deleteOrder";

class Orders extends React.Component{

  componentDidMount() {
    this.props.getOrders();
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(id){
    // this.props.deleteOrder(props,id);
    console.log(this.props);
    this.props.deleteOrder(this.props,id);
  }
  render() {
    const orders = Array.from(this.props.orders);
    console.log('this is orders')
    console.log(this.props)
    return(
      <div>
        <ul className={'order_ul'}>
          {
            orders.map((item,index)=>{
              return <OrderItem key={index} func={this.handleDelete} id={item.id} num={item.num} name={item.product.productName} price={item.product.price} unit={item.product.unit}/>
            })
          }
        </ul>
      </div>
    );
  }
}

const mapStatesToProps = state => ({
  orders: state.fetchOrdersReducer.orders,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getOrders,deleteOrder
},dispatch);

export default connect(mapStatesToProps,mapDispatchToProps)(Orders);