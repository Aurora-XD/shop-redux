import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {getProducts} from "../actions/getProducts";
import {connect} from "react-redux";
import ProductItem from "../component/ProductItem";

class Home extends Component{

  componentDidMount() {
    console.log("to get products")

    console.log(this.props)
    this.props.getProducts()
  }

  render() {
    const products = Array.from(this.props.products)
    console.log("this is product")

    console.log(products);
    return(
      <div className={'home'}>
        <ul className={'home_ul'}>
          {
            products.map((item,index)=> {
                // const {id,img, price,name,unit} = item;
                console.log(item.productName)
                // return <div>{item.name}</div>
                return <ProductItem key={index} id={item.id} name={item.productName} price={item.price} unit={item.unit} imgUrl={item.imageURL}/>
              }
            )
          }
        </ul>
      </div>
    );
  }
}

const mapStatesToProps = state => ({
  products: state.fetchProductsReducer.products,
});

//绑定组件和actioncreater
const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
  }
  , dispatch);

export default connect(mapStatesToProps,mapDispatchToProps)(Home);