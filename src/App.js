import React from 'react';
import './App.css';

import {BrowserRouter as Router, NavLink, Route, Switch} from "react-router-dom";
import Home from "./pages/Home";
import CreateProduct from "./pages/CreateProduct";
import Orders from "./pages/Orders";


function App() {
  return (
    <div className="App">
      <div>
        <h1>商城</h1>
      </div>

      <Router>
        <NavLink to='/'>商城</NavLink>
        <NavLink to='/orders'>订单</NavLink>
        <NavLink to='/products'>添加商品</NavLink>

        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/orders' component={Orders}/>

          <Route path='/products' component={CreateProduct}/>

        </Switch>
      </Router>

    </div>
  );
}

export default App;
