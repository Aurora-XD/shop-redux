import {combineReducers} from "redux";
import fetchProductsReducer from "./fetchProductsReducer";
import createProductReducer from "./createProductReducer";
import fetchOrdersReducer from "./fetchOrdersReducer";

const reducers = combineReducers({
  fetchProductsReducer,
  createProductReducer,
  fetchOrdersReducer
});
export default reducers;