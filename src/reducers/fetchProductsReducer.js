const initState = {
  products: [],
  productItem: ''
}

export default (states = initState, action) => {
  switch (action.type) {
    case 'GET_PRODUCTS':
      return {
        ...states,
        products: action.products
      };
    case 'GET_ONE_PRODUCT':
      return {
        ...states,
        productItem: action.product
      }
    default:
      return states;
  }
}