const inistate = {
  orders: ''
}

export default (states = inistate,action) => {

  switch (action.type) {
    case 'GET_ORDERS':
      return{
        ...states,
        orders: action.orders
      };
    case 'ADD_ORDER':
      return {
        ...states,
      };
    case 'DELETE_ORDER':
      return {
        ...states,
      };
    default:
      return states;
  }
}