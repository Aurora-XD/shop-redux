const initState = {
  name: '',
  price: '',
  unit: '',
  image: ''
}

export default (states = initState, action) => {
  switch (action.type) {
    case 'CREATE_NAME':
      return {
        ...states,
        name: action.postName
      }
    case 'CREATE_PRICE':
      return {
        ...states,
        price: action.postPrice
      }
    case 'CREATE_UNIT':
      return {
        ...states,
        unit: action.postUnit
      }
    case 'CREATE_IMAGE':
      return {
        ...states,
        image:action.postImage
      }
    default:
      return states;
  }
}