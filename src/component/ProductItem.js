import React from "react";
import {bindActionCreators} from "redux";
import {addOrders} from "../actions/addOrders";
import {connect} from "react-redux";

class ProductItem extends React.Component{

  constructor(props){
    super(props)
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick(id){
    console.log("yangd")
    this.props.addOrders(id)
  }

  render() {
    return(
      <li className={'productItem'}>
        <img src={this.props.imgUrl} alt=""/>
        <div className={'itemInfo'}>
          <h3>{this.props.name}</h3>
          <div>单价:{this.props.price}元/瓶</div>
        </div>
        <div>
          <button onClick={()=>this.handleOnClick(this.props.id)}>加入订单</button>
        </div>
      </li>
    );
  }
}

const mapStatesToProps = state => ({
  orders: state.fetchOrdersReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addOrders,
},dispatch);

export default connect(mapStatesToProps,mapDispatchToProps)(ProductItem);