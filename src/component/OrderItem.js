import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteOrder} from "../actions/deleteOrder";

class OrderItem extends React.Component{

  constructor(props){
    super(props)
    // this.handleDelete = this.handleDelete.bind(this);
    this.handleOnclick = this.handleOnclick.bind(this);
  }

  // handleDelete(){
  //   // this.props.deleteOrder(props,id);
  //   console.log(this.props);
  //   this.props.deleteOrder(this.props, this.props.id);
  // }

  handleOnclick(id){
    this.props.func(id);
  }

  render() {
    return(
      <li className={'orderItem'}>
        <span>{this.props.name}       </span>
        <span>{this.props.price}        </span>
        <span>{this.props.num}        </span>
        <span>{this.props.unit}       </span>
        {/*<button onClick={()=>this.handleDelete(this.props.id)}>删除</button>*/}
        <button onClick={this.handleOnclick}>删除</button>
      </li>
    );
  }
}

const mapStatesToProps = state => ({
  orders: state.fetchOrdersReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteOrder,
},dispatch);

export default connect(mapStatesToProps,mapDispatchToProps)(OrderItem);