
export const postProduct = (props,name,price,unit,image) => (dispatch) => {
  console.log("this is post")
  fetch('http://localhost:8080/api/products',{
    method:'POST',
    body:JSON.stringify({prodName:name,price:price,prodUnit:unit,imageURL:image}),
    headers: {
      'content-type': 'application/json'
    }
  })
    .then()
};

export const createProductName = (name) => (dispatch) => {

  dispatch({
    type: 'CREATE_NAME',
    postName: name
  });
};

export const createProductPrice = (price) => (dispatch) => {

  dispatch({
    type: 'CREATE_PRICE',
    postPrice: price
  });
};

export const createProductUnit = (unit) => (dispatch) => {

  dispatch({
    type: 'CREATE_UNIT',
    postUnit: unit
  });
};

export const createProductImageUrl = (imgUrl) => (dispatch) => {

  dispatch({
    type: 'CREATE_IMAGE',
    postImage: imgUrl
  });
};