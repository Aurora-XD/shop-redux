export const getProducts = () => (dispatch) => {
  fetch('http://localhost:8080/api/products').then(response => response.json())
    .then(result =>
    {
      console.log(result);
      dispatch({
        type: 'GET_PRODUCTS',
        products: result
      })
    }
    )
};

export const getProductById = (id) => (dispatch) =>{
  fetch('http://localhost:8080/api/products/'+id).then(response => response.json())
    .then(result =>
      {
        console.log(result);
        dispatch({
          type: 'GET_ONE_PRODUCT',
          product: result
        })
      }
    )
};