export const deleteOrder = (props,id) => (dispatch) => {
  fetch('http://localhost:8080/api/orders',{
    method: 'DELETE',
    body: id,
    headers:{
      'content-type': 'application/json'
    }
  }).then(()=>{
    dispatch({
      type: 'DELETE_ORDER'
    });
    props.history.push('/');
  })
}

