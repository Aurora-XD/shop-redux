export const getOrders = () => (dispatch) => {
  fetch('http://localhost:8080/api/orders').then(response => response.json())
    .then(result =>
      {
        console.log(result);
        dispatch({
          type: 'GET_ORDERS',
          orders: result
        })
      }
    )
}